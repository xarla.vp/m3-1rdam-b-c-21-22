import java.util.Scanner;

public class Arrodoniment {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner (System.in);
		float notaExamen = 0;
		int notaFinal = 0;
		int decimals = 0; 
		
		System.out.println("Introdueix la nota de l'examen (fins a 2 xifres decimals): ");
		notaExamen = lector.nextFloat();
		
		notaFinal = (int) notaExamen; // el casting trunca els decimals
		System.out.println("Nota final sense arrodonir: " + notaFinal); 
		
		notaExamen = notaExamen * 100; //amb aix� es converteix en un enter entre 0 i 999
		decimals = (int) notaExamen % 100;
		System.out.println("Els decimals s�n: " + decimals); 
		
		if (decimals >= 50) {
			notaFinal = notaFinal +1;
		}
		System.out.println("Nota final definitiva: " + notaFinal); 
		lector.close();
	}
}
