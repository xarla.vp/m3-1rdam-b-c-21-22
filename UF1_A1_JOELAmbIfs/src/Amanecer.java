import java.util.Scanner;

public class Amanecer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner (System.in);
		int segons, dies, residu;
		
		System.out.println("Introdueix el nombre de segons: ");
		segons = lector.nextInt();  
		/* Un dia t� 60*60*24 = 86400 segons*/
		
		dies = segons / 86400 + 1;
		/* el resultat de la divisi� pot tenir decimals per� al ficar el resultat en un int els trunca !!*/
		
		residu = segons % 86400;
		/* calcula el nombre de segons que sobren dels dies complets, estar� entre 0 i 86399*/
		
		/* mig dia s�n 43200 segons*/
		if (residu < 43200) {
			System.out.println("mat� del dia " + dies);
		}
		else {
			System.out.println("nit del dia " + dies);
		}
		lector.close();
	}

}